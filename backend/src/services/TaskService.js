const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

async function createTask(data) {
  return await prisma.task.create({ data });
}

async function getTasksByProjectId(projectId) {
  const numericProjectId = parseInt(projectId, 10);
  return await prisma.task.findMany({ where: { projectId: numericProjectId } });
}

module.exports = { createTask, getTasksByProjectId };