const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

async function createProject(data) {
  return await prisma.project.create({ data });
}

async function getAllProjects() {
  return await prisma.project.findMany({
    include: { tasks: true }
  });
}

module.exports = { createProject, getAllProjects };