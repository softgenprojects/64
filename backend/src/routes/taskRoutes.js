const express = require('express');
const { createTaskController, getTasksByProjectIdController } = require('@controllers/TaskController');

const router = express.Router();

router.post('/', createTaskController);
router.get('/byProject', getTasksByProjectIdController);

module.exports = router;