const express = require('express');
const { createProjectController, getAllProjectsController } = require('@controllers/ProjectController');

const router = express.Router();

router.post('/', createProjectController);
router.get('/', getAllProjectsController);

module.exports = router;