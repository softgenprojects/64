const asyncHandler = require('express-async-handler');
const { createProject, getAllProjects } = require('@services/ProjectService');

const createProjectController = asyncHandler(async (req, res) => {
  const project = await createProject(req.body);
  res.status(201).json(project);
});

const getAllProjectsController = asyncHandler(async (req, res) => {
  const projects = await getAllProjects();
  res.status(200).json(projects);
});

module.exports = { createProjectController, getAllProjectsController };