const asyncHandler = require('express-async-handler');
const { createTask, getTasksByProjectId } = require('@services/TaskService');

const createTaskController = asyncHandler(async (req, res) => {
  const task = await createTask(req.body);
  res.status(201).json(task);
});

const getTasksByProjectIdController = asyncHandler(async (req, res) => {
  const tasks = await getTasksByProjectId(req.query.projectId);
  res.status(200).json(tasks);
});

module.exports = { createTaskController, getTasksByProjectIdController };